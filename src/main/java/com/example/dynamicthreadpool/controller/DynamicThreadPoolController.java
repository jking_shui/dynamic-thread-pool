package com.example.dynamicthreadpool.controller;

import com.example.dynamicthreadpool.core.DynamicThreadPoolRegistry;
import com.example.dynamicthreadpool.core.common.dto.ThreadPoolProperties;
import com.example.dynamicthreadpool.core.refresh.DynamicThreadPoolRefresher;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author shuichangrong
 * @date 2023年01月24日 15:28
 */
@RestController
@AllArgsConstructor
public class DynamicThreadPoolController {

    @PostMapping("/refresh")
    public String refresh(@RequestBody ThreadPoolProperties threadPoolProperties){
        if (threadPoolProperties.getPoolName() == null
                || threadPoolProperties.getCorePoolSize() == null
                || threadPoolProperties.getMaximumPoolSize() == null
                || threadPoolProperties.getKeepAliveTime() < 0
        ) {
            return "fail";
        }

        DynamicThreadPoolRefresher.refresh(threadPoolProperties);
        return "success";
    }

    @GetMapping("/test/{poolName}/{num}")
    public String runTest(@PathVariable String poolName, @PathVariable Integer num){
        for (int i = 0; i < num; i++) {
            DynamicThreadPoolRegistry.getThreadPoolExecutor(poolName).execute(()->{
                System.out.println("执行线程：" + Thread.currentThread().getName());
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
        }
        return "success";
    }
}
