package com.example.dynamicthreadpool.config;

import com.example.dynamicthreadpool.core.spring.DynamicThreadPool;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
public class ThreadPoolConfiguration {

    /**
     * 通过{@link DynamicThreadPool} 注解定义普通juc线程池，会享受到该框架监控功能，注解名称优先级高于方法名
     *
     * @return 线程池实例
     */
    @DynamicThreadPool("dtp1")
    public ThreadPoolExecutor dtp1() {
        return (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
    }

    @DynamicThreadPool("dtp2")
    public ThreadPoolExecutor dtp2() {
        return (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
    }

}
