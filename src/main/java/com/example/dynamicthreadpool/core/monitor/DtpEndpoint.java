package com.example.dynamicthreadpool.core.monitor;

import cn.hutool.core.io.FileUtil;
import cn.hutool.system.RuntimeInfo;
import com.example.dynamicthreadpool.core.DynamicThreadPoolRegistry;
import com.example.dynamicthreadpool.core.common.MetricsConverter;
import com.example.dynamicthreadpool.core.common.dto.JvmStats;
import com.example.dynamicthreadpool.core.common.dto.Metrics;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Endpoint(id = "dynamic-thread-pool")
public class DtpEndpoint {

    @ReadOperation
    public List<Metrics> invoke() {
        List<Metrics> metricsList = new ArrayList<>();

        DynamicThreadPoolRegistry.listAllPoolNames().forEach(e-> metricsList.add(MetricsConverter.convert(e, DynamicThreadPoolRegistry.getThreadPoolExecutor(e))));

        JvmStats jvmStats = new JvmStats();
        RuntimeInfo runtimeInfo = new RuntimeInfo();
        jvmStats.setMaxMemory(FileUtil.readableFileSize(runtimeInfo.getMaxMemory()));
        jvmStats.setTotalMemory(FileUtil.readableFileSize(runtimeInfo.getTotalMemory()));
        jvmStats.setFreeMemory(FileUtil.readableFileSize(runtimeInfo.getFreeMemory()));
        jvmStats.setUsableMemory(FileUtil.readableFileSize(runtimeInfo.getUsableMemory()));
        metricsList.add(jvmStats);
        return metricsList;
    }
}
