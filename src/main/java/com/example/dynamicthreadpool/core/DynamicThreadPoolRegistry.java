package com.example.dynamicthreadpool.core;

import com.example.dynamicthreadpool.core.common.dto.ThreadPoolProperties;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author shuichangrong
 * @date 2023年01月29日 14:33
 */
@Slf4j
public class DynamicThreadPoolRegistry {

    private static final Map<String, ThreadPoolExecutor> REGISTRY = new ConcurrentHashMap<>();

    public static List<String> listAllPoolNames(){
        return new ArrayList<>(REGISTRY.keySet());
    }

    public static ThreadPoolExecutor getThreadPoolExecutor(String poolName) {
        return REGISTRY.get(poolName);
    }

    public static void register(String poolName, ThreadPoolExecutor threadPoolExecutor){
        REGISTRY.putIfAbsent(poolName, threadPoolExecutor);
    }

    public static void refresh(String poolName, ThreadPoolProperties threadPoolProperties){
        ThreadPoolExecutor threadPoolExecutor = REGISTRY.get(poolName);
        if(Objects.isNull(threadPoolExecutor)){
            log.warn("threadPoolExecutor is null");
            return;
        }

        refresh(threadPoolExecutor, threadPoolProperties);
    }

    private static void refresh(ThreadPoolExecutor threadPoolExecutor, ThreadPoolProperties threadPoolProperties) {
        if (threadPoolProperties.getCorePoolSize() < 0
                || threadPoolProperties.getMaximumPoolSize() <= 0
                || threadPoolProperties.getMaximumPoolSize() < threadPoolProperties.getCorePoolSize()
                || threadPoolProperties.getKeepAliveTime() < 0) {
            log.warn("threadPoolProperties error");
            return;
        }

        threadPoolExecutor.setCorePoolSize(threadPoolProperties.getCorePoolSize());
        threadPoolExecutor.setMaximumPoolSize(threadPoolProperties.getMaximumPoolSize());
        threadPoolExecutor.setKeepAliveTime(threadPoolProperties.getKeepAliveTime(), threadPoolProperties.getUnit());
    }

}
