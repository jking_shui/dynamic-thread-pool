package com.example.dynamicthreadpool.core.refresh;

import com.example.dynamicthreadpool.core.DynamicThreadPoolRegistry;
import com.example.dynamicthreadpool.core.common.dto.ThreadPoolProperties;

/**
 * @author shuichangrong
 * @date 2023年01月29日 16:46
 */
public class DynamicThreadPoolRefresher {

    public static void refresh(ThreadPoolProperties threadPoolProperties){
        DynamicThreadPoolRegistry.refresh(threadPoolProperties.getPoolName(), threadPoolProperties);
    }
}
