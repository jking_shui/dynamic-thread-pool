package com.example.dynamicthreadpool.core.common;


import com.example.dynamicthreadpool.core.common.dto.ThreadPoolStats;

import java.util.concurrent.ThreadPoolExecutor;

public class MetricsConverter {

    private MetricsConverter() {}

    public static ThreadPoolStats convert(String poolName, ThreadPoolExecutor executor) {
        return ThreadPoolStats.builder()
                .poolName(poolName)
                .corePoolSize(executor.getCorePoolSize())
                .maximumPoolSize(executor.getMaximumPoolSize())
                .poolSize(executor.getPoolSize())
                .activeCount(executor.getActiveCount())
                .taskCount(executor.getTaskCount())
                .queueType(executor.getQueue().getClass().getSimpleName())
                .queueCapacity(executor.getQueue().size() + executor.getQueue().remainingCapacity())
                .queueSize(executor.getQueue().size())
                .queueRemainingCapacity(executor.getQueue().remainingCapacity())
                .completedTaskCount(executor.getCompletedTaskCount())
                .largestPoolSize(executor.getLargestPoolSize())
                .waitTaskCount(executor.getQueue().size())
                .build();
    }
}
