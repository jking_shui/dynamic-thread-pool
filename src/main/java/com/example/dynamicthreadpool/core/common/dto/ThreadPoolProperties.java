package com.example.dynamicthreadpool.core.common.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * @author shuichangrong
 * @date 2023年01月24日 15:36
 */
@Data
public class ThreadPoolProperties implements Serializable {
    private String poolName;
    private Integer corePoolSize;
    private Integer maximumPoolSize;
    private Long keepAliveTime = 30L;
    private TimeUnit unit = TimeUnit.SECONDS;
}
