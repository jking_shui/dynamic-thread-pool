package com.example.dynamicthreadpool.core.spring;

import org.springframework.context.annotation.Bean;

import java.lang.annotation.*;

/**
 * @author shuichangrong
 * @date 2023年01月29日 14:33
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Bean
public @interface DynamicThreadPool {

    /**
     * Thread pool name, has higher priority than @Bean annotated method name.
     *
     * @return name
     */
    String value() default "";
}
