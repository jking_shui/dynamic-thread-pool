package com.example.dynamicthreadpool.core.spring;

import com.example.dynamicthreadpool.core.DynamicThreadPoolRegistry;
import com.example.dynamicthreadpool.core.common.ApplicationContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.lang.NonNull;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Objects;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author shuichangrong
 * @date 2023年01月29日 14:33
 */
@Slf4j
@DependsOn({"applicationContextHolder"})
@Component
public class DynamicThreadPoolPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(@NonNull Object bean, @NonNull String beanName) throws BeansException {
        if (!(bean instanceof ThreadPoolExecutor) && !(bean instanceof ThreadPoolTaskExecutor)) {
            return bean;
        }

        ApplicationContext applicationContext = ApplicationContextHolder.getInstance();
        DynamicThreadPool dynamicTp = applicationContext.findAnnotationOnBean(beanName, DynamicThreadPool.class);
        if (Objects.isNull(dynamicTp)) {
            return bean;
        }


        String poolName = StringUtils.isEmpty(dynamicTp.value())? beanName : dynamicTp.value();
        if(bean instanceof ThreadPoolExecutor){
            DynamicThreadPoolRegistry.register(poolName, (ThreadPoolExecutor)bean);
        }
        return bean;
    }
}
