# 动态线程池
不用重启服务即可修改线程池参数

## 原理
## 组件
* 线程池管理
* 监控
* 动态设置参数

### 使用技术
* 自定义注解
* BeanPostProcessor后置处理初始化
* nacos监听
* @Endpoint监控
* spring stater


## 功能
### 集成nacos配置中心


## 业界相关产品